package org.graph.json;

import java.util.HashMap;

public class LoadEdgeJSON {
	private int type;
	private int in;
	private int out;
	private HashMap<String, Object> data = new HashMap<String, Object>();

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getIn() {
		return in;
	}

	public void setIn(int in) {
		this.in = in;
	}

	public int getOut() {
		return out;
	}

	public void setOut(int out) {
		this.out = out;
	}

	public HashMap<String, Object> getData() {
		return data;
	}

	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}

}
