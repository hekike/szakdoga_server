package org.graph.json;

import java.util.List;

public class LoadJSON {

	private List<LoadNodeJSON> nodes;
	private List<LoadEdgeJSON> edges;

	public List<LoadNodeJSON> getNodes() {
		return nodes;
	}

	public void setNodes(List<LoadNodeJSON> nodes) {
		this.nodes = nodes;
	}

	public List<LoadEdgeJSON> getEdges() {
		return edges;
	}

	public void setEdges(List<LoadEdgeJSON> edges) {
		this.edges = edges;
	}

}
