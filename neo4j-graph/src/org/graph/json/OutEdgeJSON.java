package org.graph.json;

import java.util.HashMap;

public class OutEdgeJSON {
	private long id;
	private long inN;
	private long outN;
	private HashMap<String, Object> data = new HashMap<String, Object>();

	public OutEdgeJSON(long id, long in, long out, HashMap<String, Object> data) {
		super();
		this.id = id;
		this.inN = in;
		this.outN = out;
		this.data = data;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getInN() {
		return inN;
	}

	public void setInN(long inN) {
		this.inN = inN;
	}

	public long getOutN() {
		return outN;
	}

	public void setOutN(long outN) {
		this.outN = outN;
	}

	public HashMap<String, Object> getData() {
		return data;
	}

	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}

}
