package org.graph.json;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class OutJSON {
	@SerializedName("nodes")
	List<OutNodeJSON> nodeArr = new ArrayList<OutNodeJSON>();

	@SerializedName("edges")
	List<OutEdgeJSON> edgeArr = new ArrayList<OutEdgeJSON>();

	int size = 0;

	public OutJSON(List<OutNodeJSON> nodeArr, List<OutEdgeJSON> edgeArr, int size) {
		super();
		this.nodeArr = nodeArr;
		this.edgeArr = edgeArr;
		this.size = size;
	}

	public List<OutNodeJSON> getNodeArr() {
		return nodeArr;
	}

	public void setNodeArr(List<OutNodeJSON> nodeArr) {
		this.nodeArr = nodeArr;
	}

	public List<OutEdgeJSON> getEdgeArr() {
		return edgeArr;
	}

	public void setEdgeArr(List<OutEdgeJSON> edgeArr) {
		this.edgeArr = edgeArr;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
