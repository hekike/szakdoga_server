package org.graph.json;

import java.util.HashMap;

public class OutNodeJSON {
	private long id;
	private int depth;
	private String name;
	private int type;
	private HashMap<String, Object> data = new HashMap<String, Object>();

	public OutNodeJSON(long id, int depth, String name, int type, HashMap<String, Object> data) {
		super();
		this.id = id;
		this.depth = depth;
		this.name = name;
		this.type = type;
		this.data = data;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public HashMap<String, Object> getData() {
		return data;
	}

	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}

}
