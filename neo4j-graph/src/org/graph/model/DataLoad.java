package org.graph.model;

import java.util.HashMap;

import org.graph.json.LoadEdgeJSON;
import org.graph.json.LoadJSON;
import org.graph.json.LoadNodeJSON;
import org.graph.util.Consts;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.EmbeddedGraphDatabase;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class DataLoad {

	private DataLoad() {
	}

	public static void loadFromJSON(String json) {

		Gson gson = new Gson();
		LoadJSON loadData = new LoadJSON();

		try {
			loadData = gson.fromJson(json, LoadJSON.class);

		} catch (JsonSyntaxException e) {
			System.err.println(e.getMessage());
		}

		GraphDatabaseService graphDb = new EmbeddedGraphDatabase(Consts.DB_PATH);
		
		try {
			Transaction tx = graphDb.beginTx();

			try {

				// Nodes
				HashMap<Integer, Node> nodeMap = new HashMap<Integer, Node>();

				for (LoadNodeJSON n : loadData.getNodes()) {

					Node node = graphDb.createNode();
					node.setProperty("name", n.getName());
					node.setProperty("type", n.getType());

					// Data
					for (String key : n.getData().keySet()) {
						node.setProperty(key, n.getData().get(key));
					}

					nodeMap.put(n.getId(), node);
				}

				// Relationships
				for (LoadEdgeJSON e : loadData.getEdges()) {
					Relationship rel = null;

					if (e.getIn() < 1) {
						rel = graphDb.getReferenceNode().createRelationshipTo(nodeMap.get(e.getOut()),
								Consts.getRelType(e.getType()));
					} else {
						rel = nodeMap.get(e.getIn()).createRelationshipTo(nodeMap.get(e.getOut()),
								Consts.getRelType(e.getType()));
					}

					// Data
					for (String key : e.getData().keySet()) {
						rel.setProperty(key, e.getData().get(key));
					}

				}

				tx.success();
			} finally {
				tx.finish();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			graphDb.shutdown();
		}
	}

}
