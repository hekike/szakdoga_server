package org.graph.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.graph.json.OutEdgeJSON;
import org.graph.json.OutJSON;
import org.graph.json.OutNodeJSON;
import org.graph.util.Consts;
import org.graph.util.Consts.RelTypes;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.StopEvaluator;

import org.neo4j.graphdb.Traverser;
import org.neo4j.graphdb.Traverser.Order;
import org.neo4j.kernel.EmbeddedGraphDatabase;

public class DataProvider {

	private DataProvider() {
	}

	public static OutJSON getPeople() {
		List<OutNodeJSON> nodeArr = new ArrayList<OutNodeJSON>();
		List<OutEdgeJSON> edgeArr = new ArrayList<OutEdgeJSON>();

		GraphDatabaseService graphDb = new EmbeddedGraphDatabase(Consts.DB_PATH);

		try {

			Traverser nodes = graphDb.getReferenceNode().traverse(Order.BREADTH_FIRST,
					StopEvaluator.END_OF_GRAPH, ReturnableEvaluator.ALL_BUT_START_NODE, RelTypes.COMPANY,
					Direction.BOTH, RelTypes.CONTAIN, Direction.BOTH, RelTypes.HEAD_OF, Direction.BOTH,
					RelTypes.OWNER, Direction.BOTH, RelTypes.PART_OF, Direction.BOTH, RelTypes.WORK_ON,
					Direction.BOTH);

			for (Node node : nodes) {
				long nodeId = node.getId();
				String nodeName = (String) node.getProperty("name");
				int nodeDepth = nodes.currentPosition().depth();
				int nodeType = (int) node.getProperty("type");

				HashMap<String, Object> nodeData = new HashMap<String, Object>();
				for (String key : node.getPropertyKeys()) {
					if (!key.equals("name") && !key.equals("type")) {
						nodeData.put(key, node.getProperty(key));
					}
				}

				Iterable<Relationship> outArr = node.getRelationships(Direction.OUTGOING);

				for (Relationship r : outArr) {

					// Load edge data
					Long out = (Long) r.getEndNode().getId();

					HashMap<String, Object> edgeData = new HashMap<String, Object>();
					for (String key : r.getPropertyKeys()) {
						if (!key.equals("name") && !key.equals("type")) {
							edgeData.put(key, r.getProperty(key));
						}
					}
					edgeArr.add(new OutEdgeJSON(r.getId(), nodeId, out, edgeData));
				}

				OutNodeJSON nodeJSON = new OutNodeJSON(nodeId, nodeDepth, nodeName, nodeType, nodeData);
				nodeArr.add(nodeJSON);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			graphDb.shutdown();
		}

		return new OutJSON(nodeArr, edgeArr, nodeArr.size());
	}

}
