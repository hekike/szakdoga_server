package org.graph.util;

import org.neo4j.graphdb.RelationshipType;

public class Consts {
	public static final String DB_PATH = "D:/Development/tools/neo4j-community-1.6/data/graph.db";

	public static enum RelTypes implements RelationshipType {
		// 10, 20, 30
		COMPANY, PART_OF, CONTAIN,
		// 40, 50, 60
		HEAD_OF, WORK_ON, OWNER;

	}

	public static RelTypes getRelType(int code) {
		switch (code) {
		case 10:
			return RelTypes.COMPANY;
		case 20:
			return RelTypes.PART_OF;
		case 30:
			return RelTypes.CONTAIN;
		case 40:
			return RelTypes.HEAD_OF;
		case 50:
			return RelTypes.WORK_ON;
		case 60:
			return RelTypes.OWNER;
		default:
			return RelTypes.CONTAIN;
		}
	}

	//Node Type
	public static enum NodeType {
		COMPANY(10), DEPARTMENT(20), PROJECT(30), PEOPLE(40), TEHCNOLOGY(50);

		private int code;

		private NodeType(int c) {
			code = c;
		}

		public int getCode() {
			return code;
		}
	}

}
